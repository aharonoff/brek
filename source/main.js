// let controls = {
//     circle: {
//         UP: 106,
//         RIGHT: 109,
//         DOWN: 39,
//         LEFT: 38,
//         ENTER: 107
//     },
//     square: {
//         UP: 111,
//         RIGHT: 33,
//         DOWN: 12,
//         LEFT: 36,
//         ENTER: 37
//     }
// }

let controls = {
    circle: {
        UP: 87,
        RIGHT: 68,
        DOWN: 83,
        LEFT: 65,
        ENTER: 69
    },
    square: {
        UP: 38,
        RIGHT: 39,
        DOWN: 40,
        LEFT: 37,
        ENTER: 34
    }
}

// SOUND: https://erothyme.bandcamp.com/album/pinpoint-sol

let screenSpace = {
    width: undefined,
    height: undefined
}

let grid = {
    width: 18,
    height: 32,
    tileSize: 60
}

let colors = {
    black: [ 32, 32, 32 ],
    yellow: [ 225, 183, 40 ],
    red: [ 180, 24, 46 ],
    green: [ 14, 159, 143 ]
}

let time = {
    global: 0,
    stepper: 0,
    offset: 0,
    countdown: 0,
    laser: 0,
    speedIncrement: 0.00025,
    increment: 0,
    swapWindow: 0.5,
    swapTimer: 0,
    gameover: 0
}

let font
let laser
let system
let swaps = []
let obstaclesCircle = []
let obstaclesSquare = []

let gameState = {
    highscores: true,
    joining: false,
    playerCircle: {
        joined: false
    },
    playerSquare: {
        joined: false
    },
    countdown: false,
    gameplay: false,
    gameover: false,
    leaderboard: false,
    highscore: false
}

let highscores = [ 0, 0, 0, 0, 0, 0, 0, 0, 0 ]
let currentScore = 0
let yoff = 0
let generateSwaps = true

let sfx = {
    move: [],
    gameOver: undefined,
    charged: undefined,
    obstacle: undefined,
    swap: undefined,
    step: undefined,
    countdown: undefined,
    leaderboard: undefined,
    highscore: undefined,
    music: undefined
}

let sliders = []

var points = []
var accel = 0.9
var gravity = 0.0
var bounce = -1

class swapAnimation {
    constructor( s, e ) {
        this.s = s
        this.e = e
        this.isHorizontal
        this.state = 0
        this.size
        this.speed = 0.01

        if ( this.s.x === this.e.x ) {
            this.isHorizontal = false
            this.size = Math.abs( this.s.y - this.e.y )
        }

        if ( this.s.y === this.e.y ) {
            this.isHorizontal = true
            this.size = Math.abs( this.s.x - this.e.x )
        }
    }
    render() {
        push()
        translate( windowWidth * 0.5 - 0.5 * grid.tileSize * grid.width + ( this.s.x + this.e.x ) * 0.5 * grid.tileSize + grid.tileSize * 0.5, 0 )
        translate( 0, windowHeight * 0.5 + 0.5 * grid.tileSize * grid.height - ( this.s.y + this.e.y ) * 0.5 * grid.tileSize - grid.tileSize * 0.5 )
        translate( 0, grid.tileSize * time.offset )
        
        fill( colors.yellow )
        stroke( colors.black )
        strokeWeight( grid.tileSize * 0.125 )
        if ( this.isHorizontal ) {
            rect( 0, 0, this.state * this.size * grid.tileSize, grid.tileSize * 0.5 )
        } else {
            rect( 0, 0, grid.tileSize * 0.5, this.state * this.size * grid.tileSize )
        }
        pop()
        
        this.state += deltaTime * this.speed
    }
}

class Slider {
    constructor( s, e, type ) {
        this.s = s
        this.e = e
        this.state = 0
        this.speed = 0.005
        this.type = type
    }
    
    render() {
        push()
        translate( windowWidth * 0.5 - 0.5 * grid.tileSize * grid.width + this.s.x * grid.tileSize + grid.tileSize * 0.5, 0 )
        translate( 0, windowHeight * 0.5 + 0.5 * grid.tileSize * grid.height - this.s.y * grid.tileSize - grid.tileSize * 0.5 )
        translate( 0, grid.tileSize * time.offset )
        
        fill( colors.yellow )
        stroke( colors.black )
        strokeWeight( grid.tileSize * 0.125 )
        if ( this.type === 'CIRCLE') {
            circle( this.state * grid.tileSize * ( this.s.x - this.e.x ), this.state * grid.tileSize * ( this.s.y - this.e.y ), grid.tileSize * 0.5 )
        } else {
            square( this.state * grid.tileSize * ( this.s.x - this.e.x ), this.state * grid.tileSize * ( this.s.y - this.e.y ), grid.tileSize * 0.5 )
        }
        pop()
        
        this.state += deltaTime * this.speed
    }
}

class Path {
    constructor( tileStart, tileEnd ) {
        this.tileStart = tileStart
        this.tileEnd = tileEnd
        
        this.x = ( this.tileStart.x + this.tileEnd.x ) * 0.5
        this.y = ( this.tileStart.y + this.tileEnd.y ) * 0.5
        
        if ( this.tileStart.x === this.tileEnd.x ) {
            this.size = Math.abs( this.tileStart.y - this.tileEnd.y )
            this.isHorizontal = false
        } else {
            this.size = Math.abs( this.tileStart.x - this.tileEnd.x )
            this.isHorizontal = true
        }
    }
    
    render() {
        push()
        translate( 0, 0 )
        
        noFill()
        stroke( colors.black )
        strokeWeight( grid.tileSize * 0.125 )
        
        line( 0, 0, 0, 0 )
        pop()
    }
}

class Obstacle {
    constructor( x1, y1, x2, y2 ) {
        this.x1 = x1
        this.y1 = y1
        this.x2 = x2
        this.y2 = y2
        
        this.cx = 0.5 * ( this.x1 + this.x2 )
        this.cy = 0.5 * ( this.y1 + this.y2 )
        
    }
    render() {
        if ( playerCircle.swapAnimation === undefined && playerSquare.swapAnimation === undefined ) {
            push()
            translate( windowWidth * 0.5 - 0.5 * grid.tileSize * grid.width + this.cx * grid.tileSize + grid.tileSize * 0.5, 0 )
            translate( 0, windowHeight * 0.5 + 0.5 * grid.tileSize * grid.height - this.cy * grid.tileSize - grid.tileSize * 0.5 )
            translate( 0, grid.tileSize * time.offset )
            rotate( 2 * sin( time.global * 6 ) )
            
            noStroke()
            fill( colors.red )
            // rect( 0, 0, grid.tileSize * 0.75 )
            star( 0, 0, grid.tileSize * 0.75, grid.tileSize * 0.25, 4 )
            
            fill( colors.yellow )
            // rect( 0, 0, grid.tileSize * 0.5 )
            star( 0, 0, grid.tileSize * 0.5, grid.tileSize * 0.125, 4 )
            pop()
        }
    }
}

class Player {
    constructor( x, y, type ) {
        this.x = x
        this.y = y
        this.type = type
        this.tiles = []
        this.shaking = false
        this.isCharged = false
        this.obstacleAhead = false
        this.timeToSwap = false
        this.slider = undefined
        this.swapTime = undefined
        this.swapAnimation = undefined
    }
    render() {
        if ( this.slider === undefined && this.swapAnimation === undefined ) {
            push()
            translate( windowWidth * 0.5 - 0.5 * grid.tileSize * grid.width, 0 )
            translate( grid.tileSize * 0.5 + this.x * grid.tileSize, windowHeight * 0.5 + 0.5 * grid.tileSize * grid.height - grid.tileSize * 0.5 - this.y * grid.tileSize )
            translate( 0, grid.tileSize * time.offset )
            if ( this.shaking ) {
                translate( ( Math.random() - 0.5 ) * grid.tileSize * 0.125, ( Math.random() - 0.5 ) * grid.tileSize * 0.125 )
            }
            noStroke()
            fill( colors.yellow )
            if ( this.type === 'CIRCLE' ) {
                circle( 0, 0, grid.tileSize * 0.5 + grid.tileSize * 0.125 * sin( time.global * 6 ) )
            } 
            if ( this.type === 'SQUARE' ) {
                rect( 0, 0, grid.tileSize * 0.5 + grid.tileSize * 0.125 * sin( time.global * 6 ) )
            }
            fill( colors.black )
            noStroke()
            if ( this.isCharged ) {
                scale( 0.5 + 0.25 * sin( time.global * 6 ), 0.5 + 0.25 * sin( time.global * 6 ) )
                rect( 0, 0, grid.tileSize * 0.125, grid.tileSize * 0.5 )
                rect( 0, 0, grid.tileSize * 0.5, grid.tileSize * 0.125 )
            }
            pop()
        }
    }
}

class Tile {
    constructor( x, y, type, direction, isCharged ) {
        this.x = x
        this.y = y
        this.type = type
        this.direction = direction
        this.maxStepToRight
        this.maxStepToDown
        this.maxStepToLeft
        this.maxStepToUp
        this.maxDistance = 11
        this.boolean = ( this.type === 'CIRCLE' )
        
        // this.calculateOptions()
        
        this.neighbours = {
            up: [],
            left: [],
            down: [],
            right: []
        }
        this.closestNeighbour = {
            up: undefined,
            left: undefined,
            down: undefined,
            right: undefined
        }
        this.possibility = 0.1
        this.isCharged = isCharged
        this.swapPossibility = 1.0
        this.isSwapTile = false
    }
    
    calculateOptions() {
        this.neighbours = {
            up: [],
            left: [],
            down: [],
            right: []
        }
        this.closestNeighbour = {
            up: undefined,
            left: undefined,
            down: undefined,
            right: undefined
        }
        
        this.maxStepToRight = grid.width - this.x - 2
        this.maxStepToDown = this.y - 5 - Math.floor( time.offset )
        this.maxStepToLeft = this.x - 1
        this.maxStepToUp = grid.height - this.y - 4 + Math.floor( time.offset )
        // this.maxStepToUp = this.maxDistance
        
        if ( this.maxStepToRight < 2 ) this.maxStepToRight = false
        if ( this.maxStepToDown < 2 ) this.maxStepToDown = false
        if ( this.maxStepToLeft < 2 ) this.maxStepToLeft = false
        if ( this.maxStepToUp < 2 ) this.maxStepToUp = false
        
        if ( this.maxStepToRight > this.maxDistance ) this.maxStepToRight = this.maxDistance
        if ( this.maxStepToDown > this.maxDistance ) this.maxStepToDown = this.maxDistance
        if ( this.maxStepToLeft > this.maxDistance ) this.maxStepToLeft = this.maxDistance
        if ( this.maxStepToUp > this.maxDistance ) this.maxStepToUp = this.maxDistance
        
        if ( this.type === 'CIRCLE' ) {
            for ( let i = 0; i < tilesSquare.length; i++ ) {
                if ( tilesSquare[ i ].x === this.x && tilesSquare[ i ].y > this.y ) {
                    this.neighbours.up.push( tilesSquare[ i ].y )
                }
                if ( tilesSquare[ i ].x === this.x && tilesSquare[ i ].y < this.y ) {
                    this.neighbours.down.push( tilesSquare[ i ].y )
                }
                if ( tilesSquare[ i ].y === this.y && tilesSquare[ i ].x > this.x ) {
                    this.neighbours.right.push( tilesSquare[ i ].x )
                }
                if ( tilesSquare[ i ].y === this.y && tilesSquare[ i ].x < this.x ) {
                    this.neighbours.left.push( tilesSquare[ i ].x )
                }
            }
        }
        
        if ( this.type === 'SQUARE' ) {
            for ( let i = 0; i < tilesCircle.length; i++ ) {
                if ( tilesCircle[ i ].x === this.x && tilesCircle[ i ].y > this.y ) {
                    this.neighbours.up.push( tilesCircle[ i ].y )
                }
                if ( tilesCircle[ i ].x === this.x && tilesCircle[ i ].y < this.y ) {
                    this.neighbours.down.push( tilesCircle[ i ].y )
                }
                if ( tilesCircle[ i ].y === this.y && tilesCircle[ i ].x > this.x ) {
                    this.neighbours.right.push( tilesCircle[ i ].x )
                }
                if ( tilesCircle[ i ].y === this.y && tilesCircle[ i ].x < this.x ) {
                    this.neighbours.left.push( tilesCircle[ i ].x )
                }
            }
        }
        
        if ( this.neighbours.up.length !== 0 ) {
            this.closestNeighbour.up = Math.min( ...this.neighbours.up )
        } else {
            this.closestNeighbour.up = false
        }
        if ( this.neighbours.left.length !== 0 ) {
            this.closestNeighbour.left = Math.max( ...this.neighbours.left )
        } else {
            this.closestNeighbour.left = false
        }
        if ( this.neighbours.down.length !== 0 ) {
            this.closestNeighbour.down = Math.max( ...this.neighbours.down )
        } else {
            this.closestNeighbour.down = false
        }
        if ( this.neighbours.right.length !== 0 ) {
            this.closestNeighbour.right = Math.min( ...this.neighbours.right )
        } else {
            this.closestNeighbour.right = false
        }
        
        if ( this.closestNeighbour.up !== false ) {
            if ( this.maxStepToUp > this.closestNeighbour.up - this.y ) {
                this.maxStepToUp = this.closestNeighbour.up - this.y - 2
            }
        }
        if ( this.closestNeighbour.left !== false ) {
            if ( this.maxStepToLeft > this.closestNeighbour.left ) {
                this.maxStepToLeft = this.x - this.closestNeighbour.left - 2
            }
        }
        if ( this.closestNeighbour.down !== false ) {
            if ( this.maxStepToDown > this.y - this.closestNeighbour.down ) {
                this.maxStepToDown = this.y - this.closestNeighbour.down - 2
            }
        }
        if ( this.closestNeighbour.right !== false ) {
            if ( this.maxStepToRight > this.closestNeighbour.right - this.x ) {
                this.maxStepToRight = this.closestNeighbour.right - this.x - 2
            }
        }
        
        if ( this.direction === 'UP' ) {
            this.maxStepToUp = 0
        }
        if ( this.direction === 'RIGHT' ) {
            this.maxStepToRight = 0
        }
        if ( this.direction === 'DOWN' ) {
            this.maxStepToDown = 0
        }
        if ( this.direction === 'LEFT' ) {
            this.maxStepToLeft = 0
        }
        
        // console.log( this.maxStepToUp )
        // console.log( this.maxStepToRight )
        // console.log( this.maxStepToDown )
        // console.log( this.maxStepToLeft )
        
        // console.log( this.closestNeighbour )
    }
    
    chooseOption() {
        this.calculateOptions()
        
        let options = []
        // options to right
        if ( this.direction !== 'RIGHT' ) {
            for ( let i = 0; i < this.maxStepToRight - 1; i++ ) {
                options.push( [ this.maxStepToRight - i, 0 ] )
            }
        }
        // options to down
        if ( this.direction !== 'DOWN' ) {
            for ( let i = 0; i < this.maxStepToDown - 1; i++ ) {
                options.push( [ 0, -( this.maxStepToDown - i ) ] )
            }
        }
        // options to left
        if ( this.direction !== 'LEFT' ) {
            for ( let i = 0; i < this.maxStepToLeft - 1; i++ ) {
                options.push( [ -( this.maxStepToLeft - i ), 0 ] )
            }
        }
        // options to up
        if ( this.direction !== 'UP') {
            for ( let i = 0; i < this.maxStepToUp - 1; i++ ) {
                options.push( [ 0, this.maxStepToUp - i ] )
            }
        }
        // console.log( options )
        options = shuffle( options )
        
        let randomOption = []
        randomOption = options[ Math.floor( Math.random() * options.length ) ]
        
        // console.log( randomOption )
        
        // let distanceX = this.x - randomOption[ 0 ]
        // let distanceY = this.y - randomOption[ 1 ]
        if ( randomOption[ 0 ] !== 0 ) {
            if ( randomOption[ 0 ] < 0 ) {
                this.direction = 'RIGHT'
            } else {
                this.direction = 'LEFT'
            }
        } else {
            if ( randomOption[ 1 ] < 0 ) {
                this.direction = 'UP'
            } else {
                this.direction = 'DOWN'
            }
        }
        
        // console.log( this.direction )
        let isCharged
        if ( this.type === 'CIRCLE' ) {
            let chargeArray = []
            for ( let i = 0; i < tilesCircle.length; i++ ) {
                chargeArray.push( tilesCircle[ i ].isCharged )
            }
            let generateChargedTile = allAreFalse( chargeArray )
            if ( generateChargedTile && !playerCircle.isCharged ) {
                if ( Math.random() <= this.possibility ) {
                    isCharged = true
                } else {
                    isCharged = false
                }       
            } else {
                isCharged = false
            }
            if ( swaps.length === 0 ) {
                tilesCircle.push( new Tile( this.x + randomOption[ 0 ], this.y + randomOption[ 1 ], 'CIRCLE', this.direction, isCharged ) )
            }
            
            if ( generateSwaps && swaps.length === 0 && !playerCircle.timeToSwap && !playerSquare.timeToSwap && !playerCircle.isCharged && !playerSquare.isCharged ) {
                let lastTileCircle = tilesCircle[ tilesCircle.length - 1 ]
                let lastTileSquare = tilesSquare[ tilesSquare.length - 1 ]
                let isHorizontal
                if ( lastTileCircle.x === lastTileSquare.x && Math.abs( lastTileCircle.y - lastTileSquare.y ) >= 3 && Math.abs( lastTileCircle.y - lastTileSquare.y ) <= 7 ) {
                    isHorizontal = false
                    if ( Math.random() <= this.swapPossibility ) {
                        swaps.push( new swapArrow( lastTileCircle, lastTileSquare, isHorizontal ) )
                        playerCircle.timeToSwap = true
                        playerSquare.timeToSwap = true
                        lastTileCircle.isSwapTile = true
                        lastTileSquare.isSwapTile = true
                    }
                }
                if ( lastTileCircle.y === lastTileSquare.y && Math.abs( lastTileCircle.x - lastTileSquare.x ) >= 3 && Math.abs( lastTileCircle.y - lastTileSquare.y ) <= 7 ) {
                    isHorizontal = true
                    if ( Math.random() <= this.swapPossibility ) {
                        swaps.push( new swapArrow( lastTileCircle, lastTileSquare, isHorizontal ) )
                        playerCircle.timeToSwap = true
                        playerSquare.timeToSwap = true
                        lastTileCircle.isSwapTile = true
                        lastTileSquare.isSwapTile = true
                    }
                }
            }
        }
        
        if ( this.type === 'SQUARE' ) {
            let chargeArray = []
            for ( let i = 0; i < tilesSquare.length; i++ ) {
                chargeArray.push( tilesSquare[ i ].isCharged )
            }
            let generateChargedTile = allAreFalse( chargeArray )
            if ( generateChargedTile && !playerSquare.isCharged ) {
                if ( Math.random() <= this.possibility ) {
                    isCharged = true
                } else {
                    isCharged = false
                }       
            } else {
                isCharged = false
            } 
            if ( swaps.length === 0 ) {
                tilesSquare.push( new Tile( this.x + randomOption[ 0 ], this.y + randomOption[ 1 ], 'SQUARE', this.direction, isCharged ) )
            }
            
            if ( generateSwaps && swaps.length === 0 && !playerCircle.timeToSwap && !playerSquare.timeToSwap && !playerCircle.isCharged && !playerSquare.isCharged ) {
                let lastTileCircle = tilesCircle[ tilesCircle.length - 1 ]
                let lastTileSquare = tilesSquare[ tilesSquare.length - 1 ]
                let isHorizontal
                if ( lastTileCircle.x === lastTileSquare.x && Math.abs( lastTileCircle.y - lastTileSquare.y ) >= 3 && Math.abs( lastTileCircle.y - lastTileSquare.y ) <= 7 ) {
                    isHorizontal = false
                    if ( Math.random() <= 0.5 ) {
                        swaps.push( new swapArrow( lastTileCircle, lastTileSquare, isHorizontal ) )
                        playerCircle.timeToSwap = true
                        playerSquare.timeToSwap = true
                        lastTileCircle.isSwapTile = true
                        lastTileSquare.isSwapTile = true
                    }
                }
                if ( lastTileCircle.y === lastTileSquare.y && Math.abs( lastTileCircle.x - lastTileSquare.x ) >= 3 && Math.abs( lastTileCircle.y - lastTileSquare.y ) <= 7 ) {
                    isHorizontal = true
                    if ( Math.random() <= 0.5 ) {
                        swaps.push( new swapArrow( lastTileCircle, lastTileSquare, isHorizontal ) )
                        playerCircle.timeToSwap = true
                        playerSquare.timeToSwap = true
                        lastTileCircle.isSwapTile = true
                        lastTileSquare.isSwapTile = true
                    }
                }
            }
        }
    }
    
    render() {
        if ( playerCircle.swapAnimation === undefined && playerSquare.swapAnimation === undefined ) {
            push()
            translate( windowWidth * 0.5 - 0.5 * grid.tileSize * grid.width + this.x * grid.tileSize + 0.5 * grid.tileSize, 0 )
            translate( 0, windowHeight * 0.5 + 0.5 * grid.tileSize * grid.height - this.y * grid.tileSize - 0.5 * grid.tileSize )
            translate( 0, grid.tileSize * time.offset )
            noStroke()
            fill( 32 )
            if ( this.isSwapTile && tilesCircle.length === 1 && tilesSquare.length === 1 ) {
                if ( time.global % 2 < 1 ) {
                    square( 0, 0, grid.tileSize, grid.tileSize * this.boolean )
                } else {
                    square( 0, 0, grid.tileSize, grid.tileSize * ( 1 - this.boolean ) )
                }
            } else {
                square( 0, 0, grid.tileSize, grid.tileSize * this.boolean )
            }
            if ( this.isCharged ) {
                fill( colors.yellow )
                noStroke()
                rect( 0, 0, grid.tileSize * 0.125, grid.tileSize * 0.5 )
                rect( 0, 0, grid.tileSize * 0.5, grid.tileSize * 0.125 )
            }
            pop()
        }
    }
    
    renderOptions() {
        push()
        translate( windowWidth * 0.5 - 0.5 * grid.tileSize * grid.width, 0 )
        translate( grid.tileSize * 0.5 + this.x * grid.tileSize, windowHeight * 0.5 + 0.5 * grid.tileSize * grid.height - grid.tileSize * 0.5 - this.y * grid.tileSize )
        translate( 0, grid.tileSize * time.offset )
        noStroke()
        if ( this.type === 'CIRCLE' ) {
            fill( 255, 0.75 )
        } else {
            fill( 0, 0.75 )
        }
        // options to right
        for ( let i = 0; i < this.maxStepToRight - 1; i++ ) {
            square( ( this.maxStepToRight - i ) * grid.tileSize, 0, grid.tileSize * 0.25 * sin( time.global * 4 + i * 0.1 ), grid.tileSize * this.boolean )
        }
        // options to down
        for ( let i = 0; i < this.maxStepToDown - 1; i++ ) {
            square( 0, ( this.maxStepToDown - i ) * grid.tileSize, grid.tileSize * 0.25 * sin( time.global * 4 + i * 0.1 ), grid.tileSize * this.boolean )
        }
        // options to left
        for ( let i = 0; i < this.maxStepToLeft - 1; i++ ) {
            square( -( this.maxStepToLeft - i ) * grid.tileSize, 0, grid.tileSize * 0.25 * sin( time.global * 4 + i * 0.1 ), grid.tileSize * this.boolean )
        }
        // options to up
        for ( let i = 0; i < this.maxStepToUp - 1; i++ ) {
            square( 0, -( this.maxStepToUp - i ) * grid.tileSize, grid.tileSize * 0.25 * sin( time.global * 4 + i * 0.1 ), grid.tileSize * this.boolean )
        }
        pop()
    }
}

class Laser {
    constructor() {
        this.pos = 0
    }
    
    render() {
        push()
        translate( windowWidth * 0.5 - grid.tileSize * grid.width * 0.5, windowHeight * 0.5 + 0.5 * grid.height * grid.tileSize - grid.tileSize * 2 )
        
        // noFill()
        // stroke( colors.red ) 
        // strokeWeight( grid.tileSize * 0.25 )
        // line( 0, 0, grid.tileSize * grid.width, 0 )
        
        noFill()
        stroke( colors.red )
        strokeWeight( grid.tileSize * 0.25 )
        beginShape()
        let xoff = 0
        for ( let x = 0; x <= grid.tileSize * grid.width + 10; x += 10 ) {
            let y = map( noise( xoff, yoff ), 0, 1, -grid.tileSize * 0.1, grid.tileSize * 0.1 )
            vertex( x, y )
            xoff += 0.5
        }
        yoff += 0.05
        endShape()
        
        noFill()
        stroke( colors.red )
        strokeWeight( grid.tileSize * 0.25 )
        line( 0, 0, grid.tileSize * grid.width, 0)
        
        stroke( colors.yellow )
        strokeWeight( grid.tileSize * 0.125 )
        beginShape()
        xoff = 0
        for ( let x = 0; x <= grid.tileSize * grid.width + 10; x += 10 ) {
            let y = map( noise( xoff, yoff ), 0, 1, -grid.tileSize * 0.05, grid.tileSize * 0.05 )
            vertex( x, y )
            xoff += 0.5
        }
        yoff += 0.05
        endShape()
        
        
        stroke( colors.yellow )
        strokeWeight( grid.tileSize * 0.075 )
        line( 0, 0, grid.tileSize * grid.width, 0)
        
        stroke( colors.yellow )
        strokeWeight( grid.tileSize * 0.2 )
        
        this.pos = grid.tileSize * grid.width * abs( sin( 0.5 * Math.PI * time.laser ) )
        
        line( this.pos, 0, this.pos + grid.tileSize * 0.25, 0 )
        
        system.addParticle()
        system.run()
        
        fill( colors.black )
        noStroke()
        rect( 0, 0, grid.tileSize * 0.5 )
        
        pop()
        
        time.laser += deltaTime * 0.0005
        if ( time.laser >= 1 ) {
            time.laser = 0
        }
    }
}

let Particle = function( position ) {
    this.acceleration = createVector( -0.025, 0 )
    this.velocity = createVector( random( -1, 1 ), random( -1, 1 ) )
    this.position = position.copy()
    this.lifespan = grid.tileSize * 1.5
    this.color = Math.floor( Math.random() * 2 )
}

Particle.prototype.run = function() {
    this.update()
    this.display()
}

Particle.prototype.update = function(){
    this.velocity.add( this.acceleration )
    this.position.add( this.velocity )
    this.lifespan -= grid.tileSize * 0.05
}

Particle.prototype.display = function() {
    noFill()
    if ( this.color === 0 ) {
        stroke( colors.yellow )
    } else {
        stroke( colors.red )
    }
    strokeWeight( grid.tileSize * 0.175 )
    point( this.position.x, this.position.y )
}

Particle.prototype.isDead = function() {
    return this.lifespan < 0
}

let ParticleSystem = function( position ) {
    this.origin = position.copy()
    this.particles = []
}

ParticleSystem.prototype.addParticle = function() {
    this.particles.push( new Particle( this.origin ) )
}

ParticleSystem.prototype.run = function() {
    for ( let i = this.particles.length - 1; i >= 0; i-- ) {
        let p = this.particles[ i ]
        p.run()
        if ( p.isDead() ) {
            this.particles.splice( i, 1 )
        }
    }
}

class swapArrow {
    constructor( tileA, tileB, bool ) {
        this.tileA = tileA
        this.tileB = tileB
        this.x = 0.5 * ( this.tileA.x + this.tileB.x )
        this.y = 0.5 * ( this.tileA.y + this.tileB.y )
        this.isHorizontal = bool
        if ( this.isHorizontal ) {
            this.w = Math.abs( this.tileA.x - this.tileB.x )
        } else {
            this.w = Math.abs( this.tileA.y - this.tileB.y )
        }
    }
    
    render() {
        if ( tilesCircle.length === 1 && tilesSquare.length === 1 ) {
            push()
            translate( windowWidth * 0.5 - 0.5 * grid.tileSize * grid.width, 0 )
            translate( grid.tileSize * 0.5 + this.x * grid.tileSize, grid.tileSize * grid.height - grid.tileSize * 0.5 - this.y * grid.tileSize )
            translate( 0, grid.tileSize * time.offset )
            
            noFill()
            stroke( colors.black )
            strokeWeight( grid.tileSize * 0.125 )
            
            if ( !this.isHorizontal ) {
                rotate( Math.PI * 0.5 )
            }
            
            line( 0.5 * this.w * grid.tileSize - grid.tileSize, -grid.tileSize * 0.125, 0.5 * this.w * grid.tileSize - grid.tileSize - grid.tileSize * 0.25, -grid.tileSize * 0.375 )
            line( -0.5 * this.w * grid.tileSize + grid.tileSize, -grid.tileSize * 0.125, 0.5 * this.w * grid.tileSize - grid.tileSize, -grid.tileSize * 0.125 )
            
            line( -0.5 * this.w * grid.tileSize + grid.tileSize, grid.tileSize * 0.125, -0.5 * this.w * grid.tileSize + grid.tileSize + grid.tileSize * 0.25, grid.tileSize * 0.375 )
            line( -0.5 * this.w * grid.tileSize + grid.tileSize, grid.tileSize * 0.125, 0.5 * this.w * grid.tileSize - grid.tileSize, grid.tileSize * 0.125 )
            
            pop()
        }
    }
}

function preload() {
    font = loadFont( 'font/PixelifySans-Bold.ttf' )
    
    sfx.move.push( loadSound( 'sound/move_0.mp3' ) )
    sfx.move.push( loadSound( 'sound/move_1.mp3' ) )
    sfx.move.push( loadSound( 'sound/move_2.mp3' ) )
    
    sfx.gameOver = loadSound( 'sound/gameOver.mp3' )
    sfx.charged = loadSound( 'sound/charged.mp3' )
    sfx.obstacle = loadSound( 'sound/obstacle.mp3' )
    sfx.swap = loadSound( 'sound/swap.mp3' )
    sfx.step = loadSound( 'sound/step.mp3' )
    sfx.countdown = loadSound( 'sound/countdown.mp3' )
    sfx.leaderboard = loadSound( 'sound/leaderboard.mp3' )
    sfx.highscore = loadSound( 'sound/highscore.mp3' )

    sfx.music = loadSound( 'sound/music.mp3' )
}

function setup() {
    createCanvas( windowWidth, windowHeight )
    
    playerCircle = new Player( 5, 17, 'CIRCLE' )
    playerSquare = new Player( 12, 17, 'SQUARE' )
    
    tilesCircle = [ new Tile( 5, 17, 'CIRCLE', false ) ]
    tilesSquare = [ new Tile( 12, 17, 'SQUARE', false ) ]
    
    if ( windowWidth >= windowHeight * 9 / 16 ) {
        screenSpace.height = windowHeight 
        screenSpace.width = screenSpace.height * 9 / 16
    } else {
        screenSpace.width = windowWidth
        screenSpace.height = screenSpace.width * 16 / 9
    }
    
    grid.tileSize = screenSpace.width / grid.width
    
    tilesCircle[ tilesCircle.length - 1 ].calculateOptions()
    tilesSquare[ tilesSquare.length - 1 ].calculateOptions()
    tilesCircle[ tilesCircle.length - 1 ].chooseOption()
    tilesSquare[ tilesSquare.length - 1 ].chooseOption()
    
    tilesCircle[ tilesCircle.length - 1 ].calculateOptions()
    tilesSquare[ tilesSquare.length - 1 ].calculateOptions()
    tilesCircle[ tilesCircle.length - 1 ].chooseOption()
    tilesSquare[ tilesSquare.length - 1 ].chooseOption()
    
    tilesCircle[ tilesCircle.length - 1 ].calculateOptions()
    tilesSquare[ tilesSquare.length - 1 ].calculateOptions()
    
    laser = new Laser()
    system = new ParticleSystem( createVector( grid.tileSize * grid.width, 0 ) )
    generateSwaps = true  
    
    // sfx.music.loop()
    
    let start = createVector( 5, 17 )
    let end = createVector( 12, 17 )
    
    textFont( font )
}

function draw() {
    background( colors.green )
    rectMode( CENTER )
    textAlign( CENTER, CENTER )
    colorMode( RGB, 255, 255, 255, 1 )
    
    // GRID
    push()
    translate( windowWidth * 0.5 - 0.5 * grid.tileSize * grid.width, 0 )
    for (let i = 0; i < grid.width + 1; i++) {
        stroke( 20, 100, 100 )
        strokeWeight( 1 )
        line( 0 + i * grid.tileSize, 0, 0 + i * grid.tileSize, grid.tileSize * grid.height )
    }
    pop()
    push()
    translate( windowWidth * 0.5 - grid.tileSize * grid.width * 0.5, time.stepper * grid.tileSize + windowHeight * 0.5 - 0.5 * grid.tileSize * grid.height )
    for (let i = 0; i < grid.height + 1; i++) {
        stroke( 20, 100, 100 )
        strokeWeight( 1 )
        line( 0, 0 + i * grid.tileSize, grid.tileSize * grid.width, 0 + i * grid.tileSize )
    }
    pop()
    
    // HIGHSCORES
    if ( gameState.highscores ) {
        fill( colors.black )
        stroke( colors.yellow )
        strokeWeight( grid.tileSize * 0.25 )
        textSize( grid.tileSize * 4 )
        text( 'bRËK', windowWidth * 0.5, windowHeight * 0.5 - (grid.height - 6 ) * grid.tileSize * 0.5 )
        
        fill( colors.yellow )
        stroke( colors.black )
        strokeWeight( grid.tileSize * 0.2 )
        rect( windowWidth * 0.5, windowHeight * 0.5 + grid.tileSize * 1.25, grid.tileSize * 11, grid.tileSize * 22 )
        
        textSize( grid.tileSize * 1.75 )
        noStroke()
        fill( colors.black )
        text( 'highscores', windowWidth * 0.5, windowHeight * 0.5 - (grid.height - 15 ) * grid.tileSize * 0.5 )
        
        textSize( grid.tileSize * 1.25 )
        for ( let i = 0; i < highscores.length; i++ ) {
            text( new Intl.NumberFormat( "de-DE" ).format( highscores[ i ] ), windowWidth * 0.5, windowHeight * 0.5 - (grid.height - 21 - 4 * i ) * grid.tileSize * 0.5 )
        }

        textSize( grid.tileSize * 0.75 )
        if ( time.global % 2 < 1 ) {
            fill( colors.black )
            stroke( colors.black )
            strokeWeight( grid.tileSize * 0.2 )
            rect( windowWidth * 0.5, windowHeight * 0.5 - (grid.height - 59 ) * grid.tileSize * 0.5, grid.tileSize * 11, grid.tileSize * 1.25 )
            fill( colors.yellow )
            noStroke()
            text( 'YELLOW BUTTON TO START', windowWidth * 0.5, windowHeight * 0.5 - (grid.height - 59 ) * grid.tileSize * 0.5 - grid.tileSize * 0.125 )
        }
        
        pop()
    }
    
    // PLAYERS JOINING
    if ( gameState.joining ) {
        if ( gameState.playerCircle.joined ) {
            tilesCircle[ 0 ].render()
            playerCircle.render()
            
            fill( colors.yellow )
            stroke( colors.black )
            strokeWeight( grid.tileSize * 0.2 )
            push()
            translate( windowWidth * 0.5 - grid.tileSize * 3.5, windowHeight * 0.5 - grid.tileSize * 6 )
            rect( 0, 0, grid.tileSize * 5 )
            pop()
            
            fill( colors.black )
            noStroke()
            textSize( grid.tileSize )
            push()
            translate( windowWidth * 0.5 - grid.tileSize * 3.5, windowHeight * 0.5 - grid.tileSize * 6 )
            text( 'PLAYER\nONE\nJOINED', 0, 0 )
            pop()
        }
        if ( gameState.playerSquare.joined ) {
            tilesSquare[ 0 ].render()
            playerSquare.render()
            
            fill( colors.yellow )
            stroke( colors.black )
            strokeWeight( grid.tileSize * 0.2 )
            push()
            translate( windowWidth * 0.5 + grid.tileSize * 3.5, windowHeight * 0.5 - grid.tileSize * 6 )
            rect( 0, 0, grid.tileSize * 5 )
            pop()
            
            fill( colors.black )
            noStroke()
            textSize( grid.tileSize )
            push()
            translate( windowWidth * 0.5 + grid.tileSize * 3.5, windowHeight * 0.5 - grid.tileSize * 6 )
            text( 'PLAYER\nTWO\nJOINED', 0, 0 )
            pop()
        }

        fill( colors.yellow )
        stroke( colors.black )
        strokeWeight( grid.tileSize * 0.2 )
        push()
        translate( windowWidth * 0.5, windowHeight * 0.5 + grid.tileSize * 6 )
        rect( 0, -0.75 * grid.tileSize, grid.tileSize * 16, grid.tileSize * 7 )

        fill( colors.black )
        noStroke()
        push()
        translate( -grid.tileSize * 0.75, 0 )
        text( 'YELLOW KEY', -3.5 * grid.tileSize, -2.75 * grid.tileSize )
        text( '+', -3.5 * grid.tileSize, -2 * grid.tileSize )
        text( 'ARROWS', -3.5 * grid.tileSize, -1.25 * grid.tileSize )
        text( '=', 0, -2 * grid.tileSize )
        text( 'DESTROY SPIKE', 4.25 * grid.tileSize, -2 * grid.tileSize )
        text( 'YELLOW KEY', -3.5 * grid.tileSize, 1 * grid.tileSize )
        text( '=', 0, 1 * grid.tileSize )
        text( 'SWAP POSITION', 4.25 * grid.tileSize, 1 * grid.tileSize )
        pop()
        pop()

        textSize( grid.tileSize * 0.75 )
        if ( time.global % 2 < 1 ) {
            fill( colors.black )
            stroke( colors.black )
            strokeWeight( grid.tileSize * 0.2 )
            rect( windowWidth * 0.5, windowHeight * 0.5 - (grid.height - 59 ) * grid.tileSize * 0.5, grid.tileSize * 11, grid.tileSize * 1.25 )
            fill( colors.yellow )
            noStroke()
            text( 'YELLOW BUTTON TO START', windowWidth * 0.5, windowHeight * 0.5 - (grid.height - 59 ) * grid.tileSize * 0.5 - grid.tileSize * 0.125 )
        }
        
        if ( gameState.playerCircle.joined && gameState.playerSquare.joined ) {
            gameState.countdown = true
            if ( !sfx.countdown.isPlaying() ) {
                sfx.countdown.stop()
                sfx.countdown.play()
            }
        }
        
        if ( gameState.countdown ) {
            time.countdown += deltaTime * 0.001
            
            push()
            fill( colors.black )
            stroke( colors.yellow )
            strokeWeight( grid.tileSize * 0.25 )
            textSize( grid.tileSize * 4 )
            text( 3 - Math.floor( time.countdown ), windowWidth * 0.5, windowHeight * 0.5 - (grid.height - 6 ) * grid.tileSize * 0.5 )
            pop()
            
            if ( time.countdown >= 3.9 ) {
                gameState.joining = false
                gameState.countdown = false
                gameState.playerCircle.joined = false
                gameState.playerSquare.joined = false
                gameState.gameplay = true
                time.countdown = 0
            }
        }
    }
    
    // GAMEPLAY
    if ( gameState.gameplay ) {
        if ( tilesCircle.length < 3 ) {
            tilesCircle[ tilesCircle.length - 1 ].calculateOptions()
            tilesCircle[ tilesCircle.length - 1 ].chooseOption()
        }

        if ( tilesSquare.length < 3 ) {
            tilesSquare[ tilesSquare.length - 1 ].calculateOptions()
            tilesSquare[ tilesSquare.length - 1 ].chooseOption()
        }

        // TILE PATHS
        if ( playerCircle.swapAnimation === undefined && playerSquare.swapAnimation === undefined ) {
            stroke( colors.black )
            strokeWeight( grid.tileSize * 0.125 )
            for (let i = 0; i < tilesCircle.length - 1; i++) {
                push()
                translate( windowWidth * 0.5 - 0.5 * grid.tileSize * grid.width + tilesCircle[ i ].x * grid.tileSize + 0.5 * grid.tileSize, 0 )
                translate( 0, windowHeight * 0.5 + 0.5 * grid.tileSize * grid.height - tilesCircle[ i ].y * grid.tileSize - 0.5 * grid.tileSize )
                translate( 0, grid.tileSize * time.offset )
                line( 0, 0, (tilesCircle[ i + 1 ].x - tilesCircle[ i ].x ) * grid.tileSize, ( tilesCircle[ i ].y - tilesCircle[ i + 1 ].y ) * grid.tileSize )
                pop()
            }
            for (let i = 0; i < tilesSquare.length - 1; i++) {
                push()
                translate( windowWidth * 0.5 - 0.5 * grid.tileSize * grid.width + tilesSquare[ i ].x * grid.tileSize + 0.5 * grid.tileSize, 0 )
                translate( 0, windowHeight * 0.5 + 0.5 * grid.tileSize * grid.height - tilesSquare[ i ].y * grid.tileSize - 0.5 * grid.tileSize )
                translate( 0, grid.tileSize * time.offset )
                line( 0, 0, (tilesSquare[ i + 1 ].x - tilesSquare[ i ].x ) * grid.tileSize, ( tilesSquare[ i ].y - tilesSquare[ i + 1 ].y ) * grid.tileSize )
                pop()
            }
        }

        // SWAP ARROWS
        for ( let i = 0; i < swaps.length; i++ ) {
            swaps[ i ].render()
        } 
        
        // TILES
        for (let i = 0; i < tilesCircle.length; i++) {
            tilesCircle[ i ].render()
            // if ( i === tilesCircle.length - 1 ) {
            //     tilesCircle[ i ].renderOptions()
            // }
        }
        for (let i = 0; i < tilesSquare.length; i++) {
            tilesSquare[ i ].render()
            // if ( i === tilesSquare.length - 1 ) {
            //     tilesSquare[ i ].renderOptions()
            // }
        }
        
        // OBSTACLES
        for ( let i = 0; i < obstaclesCircle.length; i++ ) {
            obstaclesCircle[ i ].render()
        }
        for ( let i = 0; i < obstaclesSquare.length; i++ ) {
            obstaclesSquare[ i ].render()
        }
        
        // OBSTACLE EXPLOSION
        fill( colors.yellow )
        stroke( colors.red )
        strokeWeight( grid.tileSize * 0.0625 )
        for( var i = 0; i < points.length; i++ ){
            var p = points[ i ] 
            if (p.y > height || p.y < 0){
                p.ydrift = p.ydrift * bounce
            }
            if ( p.x > width || p.x < 0 ) {
                p.xdrift = p.xdrift * bounce
            }
            var diam = 0.2 * grid.tileSize + 1.5 * grid.tileSize / p.age
            ellipse( p.x, p.y, diam, diam )
            p.x += p.xdrift;
            p.y += p.ydrift;
            p.xdrift = p.xdrift * accel
            p.ydrift = p.ydrift * accel
            p.ydrift = p.ydrift + gravity
            p.age = p.age + 1
        }
        
        points = points.filter( function( p ) {
            if ( p.age > grid.tileSize * 2 ) return false
            else return true
        } )
        
        // PLAYERS
        playerCircle.render()
        playerSquare.render()
        
        // INFO BOX
        push()
        translate( windowWidth * 0.5, grid.tileSize * 1.5 )
        pop()
        
        // CURRENT HIGHSCORE
        push()
        translate( windowWidth * 0.5, windowHeight * 0.5 - 0.5 * grid.tileSize * grid.height + 3 * grid.tileSize * 0.5 )
        fill( colors.yellow )
        stroke( colors.black )
        strokeWeight( grid.tileSize * 0.25 )
        rect( 0, 0, grid.tileSize * grid.width - grid.tileSize * 0.25, grid.tileSize * 2.75 )
        textSize( grid.tileSize * 1.75 )
        noStroke()
        fill( colors.black )
        text( new Intl.NumberFormat( "de-DE" ).format( currentScore ), 0, -grid.tileSize * 0.25 )
        pop()
        
        if ( playerCircle.slider !== undefined ) {
            playerCircle.slider.render()
        }
        if ( playerCircle.slider !== undefined && playerCircle.slider.state >= 1 ) {
            playerCircle.slider = undefined
        }
        if ( playerSquare.slider !== undefined ) {
            playerSquare.slider.render()
        }
        if ( playerSquare.slider !== undefined && playerSquare.slider.state >= 1 ) {
            playerSquare.slider = undefined
        }

        if ( playerCircle.swapAnimation !== undefined ) {
            playerCircle.swapAnimation.render()
        }
        if ( playerCircle.swapAnimation !== undefined && playerCircle.swapAnimation.state >= 1 ) {
            playerCircle.swapAnimation = undefined
        }
        if ( playerSquare.swapAnimation !== undefined ) {
            playerSquare.swapAnimation.render()
        }
        if ( playerSquare.swapAnimation !== undefined && playerSquare.swapAnimation.state >= 1 ) {
            playerSquare.swapAnimation = undefined
        }
        
        time.stepper += deltaTime * time.speedIncrement
        time.offset += deltaTime * time.speedIncrement
        time.increment += deltaTime * 0.001
        
        if ( time.stepper >= 1 ) {
            time.stepper = 0
        }
        if ( time.increment >= 4 ) {
            time.increment = 0
            time.speedIncrement += 0.000025
        }
        
        if ( playerCircle.y - Math.floor( time.offset ) <= 2 || playerSquare.y - Math.floor( time.offset ) <= 2 ) {
            gameState.gameplay = false
            gameState.gameover = true
            sfx.gameOver.stop()
            sfx.gameOver.play()
            
            if ( currentScore > Math.min( ...highscores ) && currentScore < Math.max( ...highscores ) ) {
                highscores.pop()
                highscores.unshift( currentScore )
                highscores.sort( function( a, b ) { return b - a } )                
                
                gameState.leaderboard = true
                
                if ( !sfx.leaderboard.isPlaying() ) {

                    sfx.leaderboard.play()
                }                
            }
            
            if ( currentScore > Math.max( ...highscores ) ) {
                highscores.pop()
                highscores.unshift( currentScore )
                highscores.sort( function( a, b ) { return b - a } )                
                
                gameState.highscore = true
                
                if ( !sfx.highscore.isPlaying() ) {
                    sfx.highscore.play()
                }
            }
        }
        
        laser.render()
    }   
    
    if ( gameState.gameover ) {
        fill( colors.black )
        stroke( colors.yellow )
        strokeWeight( grid.tileSize * 0.25 )
        
        if ( !gameState.leaderboard && !gameState.highscore ) {
            textSize( grid.tileSize * 4 )
            text( 'GAME\nOVER', windowWidth * 0.5, windowHeight * 0.5 - (grid.height - 27 ) * grid.tileSize * 0.5 )
        }
        
        if ( gameState.leaderboard ) {
            if ( time.global % 2 < 1 ) {
                textSize( grid.tileSize * 4 )
                text( 'NEW\nSCORE', windowWidth * 0.5, windowHeight * 0.5 - (grid.height - 27 ) * grid.tileSize * 0.5 )
            } else {
                textSize( grid.tileSize * 4 )
                text( new Intl.NumberFormat( "de-DE" ).format( currentScore ), windowWidth * 0.5, windowHeight * 0.5 - (grid.height - 27 ) * grid.tileSize * 0.5 )
            }
        }
        
        if ( gameState.highscore ) {
            if ( time.global % 2 < 1 ) {
                textSize( grid.tileSize * 4 )
                text( 'NEW\nRECORD', windowWidth * 0.5, windowHeight * 0.5 - (grid.height - 27 ) * grid.tileSize * 0.5 )
            } else {
                textSize( grid.tileSize * 4 )
                text( new Intl.NumberFormat( "de-DE" ).format( currentScore ), windowWidth * 0.5, windowHeight * 0.5 - (grid.height - 27 ) * grid.tileSize * 0.5 )
            }
        }
    }

    time.global += deltaTime * 0.001
}

function keyPressed() {
    if ( gameState.highscores || gameState.joining ) {
        if ( !gameState.playerCircle.joined ) {
            if ( keyCode === controls.circle.ENTER ) {
                gameState.playerCircle.joined = true
                gameState.joining = true
                gameState.highscores = false
            }
        }
        if ( !gameState.playerSquare.joined ) {
            if ( keyCode === controls.square.ENTER ) {
                gameState.playerSquare.joined = true
                gameState.joining = true
                gameState.highscores = false
            }
        }

        currentScore = 0
        
        generateSwaps = false
        
        playerCircle.x = 5 
        playerCircle.y = 17
        playerSquare.x = 12
        playerSquare.y = 17
        
        playerSquare.slider = undefined
        playerCircle.slider = undefined

        playerSquare.swapTime = undefined
        playerCircle.swapTime = undefined
        
        tilesCircle = []
        tilesSquare = []
        tilesCircle = [ new Tile( 5, 17, 'CIRCLE', false ) ]
        tilesSquare = [ new Tile( 12, 17, 'SQUARE', false ) ]
        
        tilesCircle[ tilesCircle.length - 1 ].calculateOptions()
        tilesSquare[ tilesSquare.length - 1 ].calculateOptions()
        tilesCircle[ tilesCircle.length - 1 ].chooseOption()
        tilesSquare[ tilesSquare.length - 1 ].chooseOption()
        
        tilesCircle[ tilesCircle.length - 1 ].calculateOptions()
        tilesSquare[ tilesSquare.length - 1 ].calculateOptions()
        tilesCircle[ tilesCircle.length - 1 ].chooseOption()
        tilesSquare[ tilesSquare.length - 1 ].chooseOption()
        
        tilesCircle[ tilesCircle.length - 1 ].calculateOptions()
        tilesSquare[ tilesSquare.length - 1 ].calculateOptions()
        
        playerCircle.isCharged = false
        playerSquare.isCharged = false
        playerCircle.shaking = false
        playerSquare.shaking = false
        playerCircle.obstacleAhead = false
        playerSquare.obstacleAhead = false
        playerCircle.timeToSwap = false
        playerSquare.timeToSwap = false
        obstaclesCircle = []
        obstaclesSquare = []
        swaps = []
        
        time.stepper = 0
        time.offset = 0
        time.speedIncrement = 0.00025
        
        generateSwaps = true
        
        gameState.leaderboard = false
        gameState.highscore = false
        
        let randomStep = Math.floor( sfx.move.length * Math.random() )
        sfx.move[ randomStep ].stop()
        sfx.move[ randomStep ].play()
        // sfx.move[ Math.floor( sfx.move.length * Math.random() ) ].play()
        sfx.step.stop()
        sfx.step.play()
    }
    
    if ( gameState.gameplay ) {
        if ( keyCode === controls.square.UP ) {
            let options = []
            for ( let i = 1; i < tilesSquare.length; i++ ) {
                if ( playerSquare.x === tilesSquare[ i ].x && playerSquare.y < tilesSquare[ i ].y ) {
                    options.push( tilesSquare[ i ].y - playerSquare.y )
                }
            }
            let step = Math.min( ...options )
            if ( options.length > 0 ) {
                tilesSquare[ tilesSquare.length - 1 ].calculateOptions()
                tilesSquare[ tilesSquare.length - 1 ].chooseOption()
                
                if ( tilesSquare[ 0 ].isCharged ) {
                    playerSquare.isCharged = true
                    tilesSquare[ 0 ].isCharged = !tilesSquare[ 0 ].isCharged
                }
                if ( playerSquare.obstacleAhead ) {
                    if ( !playerSquare.shaking ) {
                        console.log( 'PLAYER TWO DIED' )
                        setTimeout( function() {
                            gameState.gameplay = false
                            gameState.gameover = true
                            sfx.gameOver.stop()
                            sfx.gameOver.play()
                            
                            if ( currentScore > Math.min( ...highscores ) && currentScore < Math.max( ...highscores ) ) {
                                highscores.pop()
                                highscores.unshift( currentScore )
                                highscores.sort( function( a, b ) { return b - a } )                                
                                
                                gameState.leaderboard = true
                                
                                sfx.leaderboard.stop()
                                sfx.leaderboard.play()
                                
                            }
                            
                            if ( currentScore > Math.max( ...highscores ) ) {
                                highscores.pop()
                                highscores.unshift( currentScore )
                                highscores.sort( function( a, b ) { return b - a } )                                
                                
                                gameState.highscore = true
                                
                                sfx.highscore.stop()
                                sfx.highscore.play()
                            }
                        }, 1 )
                    } else {
                        let posX = windowWidth * 0.5 - 0.5 * grid.tileSize * grid.width + obstaclesSquare[ 0 ].cx * grid.tileSize + grid.tileSize * 0.5
                        let posY = windowHeight * 0.5 + 0.5 * grid.tileSize * grid.height - obstaclesSquare[ 0 ].cy * grid.tileSize - grid.tileSize * 0.5 + grid.tileSize * time.offset
                        goBoom( posX, posY )

                        obstaclesSquare = []
                        playerSquare.obstacleAhead = false
                        playerSquare.isCharged = false
                        currentScore += 4000
                        
                        sfx.obstacle.stop()
                        sfx.obstacle.play()
                    }
                }
                tilesSquare.shift()
                currentScore += 1000
            } else {
                console.log( 'PLAYER TWO DIED' )
                setTimeout( function() {
                    gameState.gameplay = false
                    gameState.gameover = true
                    sfx.gameOver.stop()
                    sfx.gameOver.play()
                    
                    if ( currentScore > Math.min( ...highscores ) && currentScore < Math.max( ...highscores ) ) {
                        highscores.pop()
                        highscores.unshift( currentScore )
                        highscores.sort( function( a, b ) { return b - a } )                        
                        
                        gameState.leaderboard = true
                        
                        sfx.leaderboard.stop()
                        sfx.leaderboard.play()
                        
                    }
                    
                    if ( currentScore > Math.max( ...highscores ) ) {
                        highscores.pop()
                        highscores.unshift( currentScore )
                        highscores.sort( function( a, b ) { return b - a } )                        
                        
                        gameState.highscore = true
                        
                        sfx.highscore.stop()
                        sfx.highscore.play()
                    }
                }, 1 )
            }
            let start = createVector( playerSquare.x, playerSquare.y )
            let end = createVector( playerSquare.x, playerSquare.y + step )
            playerSquare.slider = new Slider( start, end, 'SQUARE' )
            
            playerSquare.y += step
            let randomStep = Math.floor( sfx.move.length * Math.random() )
            sfx.move[ randomStep ].stop()
            sfx.move[ randomStep ].play()
            // sfx.move[ Math.floor( sfx.move.length * Math.random() ) ].play()
            sfx.step.stop()
            sfx.step.play()
        }
        if ( keyCode === controls.square.DOWN ) {
            let options = []
            for ( let i = 1; i < tilesSquare.length; i++ ) {
                if ( playerSquare.x === tilesSquare[ i ].x && playerSquare.y > tilesSquare[ i ].y ) {
                    options.push( tilesSquare[ i ].y - playerSquare.y )
                }
            }
            let step = Math.max( ...options )
            if ( options.length > 0 ) {
                tilesSquare[ tilesSquare.length - 1 ].calculateOptions()
                tilesSquare[ tilesSquare.length - 1 ].chooseOption()
                if ( tilesSquare[ 0 ].isCharged ) {
                    playerSquare.isCharged = true
                    tilesSquare[ 0 ].isCharged = !tilesSquare[ 0 ].isCharged
                }
                if ( playerSquare.obstacleAhead ) {
                    if ( !playerSquare.shaking ) {
                        console.log( 'PLAYER TWO DIED' )
                        setTimeout( function() {
                            gameState.gameplay = false
                            gameState.gameover = true
                            sfx.gameOver.stop()
                            sfx.gameOver.play()
                            
                            if ( currentScore > Math.min( ...highscores ) && currentScore < Math.max( ...highscores ) ) {
                                highscores.pop()
                                highscores.unshift( currentScore )
                                highscores.sort( function( a, b ) { return b - a } )                                
                                
                                gameState.leaderboard = true
                                
                                sfx.leaderboard.stop()
                                sfx.leaderboard.play()
                                
                            }
                            
                            if ( currentScore > Math.max( ...highscores ) ) {
                                highscores.pop()
                                highscores.unshift( currentScore )
                                highscores.sort( function( a, b ) { return b - a } )                                
                                
                                gameState.highscore = true
                                
                                sfx.highscore.stop()
                                sfx.highscore.play()
                            }
                        }, 1 )
                    } else {
                        let posX = windowWidth * 0.5 - 0.5 * grid.tileSize * grid.width + obstaclesSquare[ 0 ].cx * grid.tileSize + grid.tileSize * 0.5
                        let posY = windowHeight * 0.5 + 0.5 * grid.tileSize * grid.height - obstaclesSquare[ 0 ].cy * grid.tileSize - grid.tileSize * 0.5 + grid.tileSize * time.offset
                        goBoom( posX, posY )

                        obstaclesSquare = []
                        playerSquare.obstacleAhead = false
                        playerSquare.isCharged = false
                        currentScore += 4000

                        sfx.obstacle.stop()
                        sfx.obstacle.play()
                    }
                }
                tilesSquare.shift()
                currentScore += 1000
            } else {
                console.log( 'PLAYER TWO DIED' )
                setTimeout( function() {
                    gameState.gameplay = false
                    gameState.gameover = true
                    sfx.gameOver.stop()
                    sfx.gameOver.play()
                    
                    if ( currentScore > Math.min( ...highscores ) && currentScore < Math.max( ...highscores ) ) {
                        highscores.pop()
                        highscores.unshift( currentScore )
                        highscores.sort( function( a, b ) { return b - a } )                        
                        
                        gameState.leaderboard = true
                        
                        sfx.leaderboard.stop()
                        sfx.leaderboard.play()
                        
                    }
                    
                    if ( currentScore > Math.max( ...highscores ) ) {
                        highscores.pop()
                        highscores.unshift( currentScore )
                        highscores.sort( function( a, b ) { return b - a } )                        
                        
                        gameState.highscore = true
                        
                        sfx.highscore.stop()
                        sfx.highscore.play()
                    }
                }, 1 )
            }
            let start = createVector( playerSquare.x, playerSquare.y )
            let end = createVector( playerSquare.x, playerSquare.y + step )
            playerSquare.slider = new Slider( start, end, 'SQUARE' )
            
            playerSquare.y += step
            let randomStep = Math.floor( sfx.move.length * Math.random() )
            sfx.move[ randomStep ].stop()
            sfx.move[ randomStep ].play()
            // sfx.move[ Math.floor( sfx.move.length * Math.random() ) ].play()
            sfx.step.stop()
            sfx.step.play()
        }
        if ( keyCode === controls.square.RIGHT && playerSquare.x < 17 ) {
            let options = []
            for ( let i = 1; i < tilesSquare.length; i++ ) {
                if ( playerSquare.y === tilesSquare[ i ].y && playerSquare.x < tilesSquare[ i ].x ) {
                    options.push( tilesSquare[ i ].x - playerSquare.x )
                }
            }
            let step = Math.min( ...options )
            if ( options.length > 0 ) {
                tilesSquare[ tilesSquare.length - 1 ].calculateOptions()
                tilesSquare[ tilesSquare.length - 1 ].chooseOption()
                if ( tilesSquare[ 0 ].isCharged ) {
                    playerSquare.isCharged = true
                    tilesSquare[ 0 ].isCharged = !tilesSquare[ 0 ].isCharged
                }
                if ( playerSquare.obstacleAhead ) {
                    if ( !playerSquare.shaking ) {
                        console.log( 'PLAYER TWO DIED' )
                        setTimeout( function() {
                            gameState.gameplay = false
                            gameState.gameover = true
                            sfx.gameOver.stop()
                            sfx.gameOver.play()
                            
                            if ( currentScore > Math.min( ...highscores ) && currentScore < Math.max( ...highscores ) ) {
                                highscores.pop()
                                highscores.unshift( currentScore )
                                highscores.sort( function( a, b ) { return b - a } )                                
                                
                                gameState.leaderboard = true
                                
                                sfx.leaderboard.stop()
                                sfx.leaderboard.play()
                                
                            }
                            
                            if ( currentScore > Math.max( ...highscores ) ) {
                                highscores.pop()
                                highscores.unshift( currentScore )
                                highscores.sort( function( a, b ) { return b - a } )                                
                                
                                gameState.highscore = true
                                
                                sfx.highscore.stop()
                                sfx.highscore.play()
                            }
                        }, 1 )
                    } else {
                        let posX = windowWidth * 0.5 - 0.5 * grid.tileSize * grid.width + obstaclesSquare[ 0 ].cx * grid.tileSize + grid.tileSize * 0.5
                        let posY = windowHeight * 0.5 + 0.5 * grid.tileSize * grid.height - obstaclesSquare[ 0 ].cy * grid.tileSize - grid.tileSize * 0.5 + grid.tileSize * time.offset
                        goBoom( posX, posY )

                        obstaclesSquare = []
                        playerSquare.obstacleAhead = false
                        playerSquare.isCharged = false
                        currentScore += 4000

                        sfx.obstacle.stop()
                        sfx.obstacle.play()
                    }
                }
                tilesSquare.shift()
                currentScore += 1000
            } else {
                console.log( 'PLAYER TWO DIED' )
                setTimeout( function() {
                    gameState.gameplay = false
                    gameState.gameover = true
                    sfx.gameOver.stop()
                    sfx.gameOver.play()
                    
                    if ( currentScore > Math.min( ...highscores ) && currentScore < Math.max( ...highscores ) ) {
                        highscores.pop()
                        highscores.unshift( currentScore )
                        highscores.sort( function( a, b ) { return b - a } )                        
                        
                        gameState.leaderboard = true
                        
                        sfx.leaderboard.stop()
                        sfx.leaderboard.play()
                        
                    }
                    
                    if ( currentScore > Math.max( ...highscores ) ) {
                        highscores.pop()
                        highscores.unshift( currentScore )
                        highscores.sort( function( a, b ) { return b - a } )                        
                        
                        gameState.highscore = true
                        
                        sfx.highscore.stop()
                        sfx.highscore.play()
                    }
                }, 1 )
            }
            let start = createVector( playerSquare.x, playerSquare.y )
            let end = createVector( playerSquare.x - step, playerSquare.y )
            playerSquare.slider = new Slider( start, end, 'SQUARE' )
            
            playerSquare.x += step
            let randomStep = Math.floor( sfx.move.length * Math.random() )
            sfx.move[ randomStep ].stop()
            sfx.move[ randomStep ].play()
            // sfx.move[ Math.floor( sfx.move.length * Math.random() ) ].play()
            sfx.step.stop()
            sfx.step.play()
        }
        if ( keyCode === controls.square.LEFT && playerSquare.x > 0 ) {
            let options = []
            for ( let i = 1; i < tilesSquare.length; i++ ) {
                if ( playerSquare.y === tilesSquare[ i ].y && playerSquare.x > tilesSquare[ i ].x ) {
                    options.push( tilesSquare[ i ].x - playerSquare.x )
                }
            }
            let step = Math.max( ...options )
            if ( options.length > 0 ) {
                tilesSquare[ tilesSquare.length - 1 ].calculateOptions()
                tilesSquare[ tilesSquare.length - 1 ].chooseOption()
                if ( tilesSquare[ 0 ].isCharged ) {
                    playerSquare.isCharged = true
                    tilesSquare[ 0 ].isCharged = !tilesSquare[ 0 ].isCharged
                }
                if ( playerSquare.obstacleAhead ) {
                    if ( !playerSquare.shaking ) {
                        console.log( 'PLAYER TWO DIED' )
                        setTimeout( function() {
                            gameState.gameplay = false
                            gameState.gameover = true
                            sfx.gameOver.stop()
                            sfx.gameOver.play()
                            
                            if ( currentScore > Math.min( ...highscores ) && currentScore < Math.max( ...highscores ) ) {
                                highscores.pop()
                                highscores.unshift( currentScore )
                                highscores.sort( function( a, b ) { return b - a } )                                
                                
                                gameState.leaderboard = true
                                
                                sfx.leaderboard.stop()
                                sfx.leaderboard.play()
                                
                            }
                            
                            if ( currentScore > Math.max( ...highscores ) ) {
                                highscores.pop()
                                highscores.unshift( currentScore )
                                highscores.sort( function( a, b ) { return b - a } )                                
                                
                                gameState.highscore = true
                                
                                sfx.highscore.stop()
                                sfx.highscore.play()
                            }
                        }, 1 )
                    } else {
                        let posX = windowWidth * 0.5 - 0.5 * grid.tileSize * grid.width + obstaclesSquare[ 0 ].cx * grid.tileSize + grid.tileSize * 0.5
                        let posY = windowHeight * 0.5 + 0.5 * grid.tileSize * grid.height - obstaclesSquare[ 0 ].cy * grid.tileSize - grid.tileSize * 0.5 + grid.tileSize * time.offset
                        goBoom( posX, posY )

                        obstaclesSquare = []
                        playerSquare.obstacleAhead = false
                        playerSquare.isCharged = false
                        currentScore += 4000

                        sfx.obstacle.stop()
                        sfx.obstacle.play()
                    }
                }
                tilesSquare.shift()
                currentScore += 1000
            } else {
                console.log( 'PLAYER TWO DIED' )
                setTimeout( function() {
                    gameState.gameplay = false
                    gameState.gameover = true
                    sfx.gameOver.stop()
                    sfx.gameOver.play()
                    
                    if ( currentScore > Math.min( ...highscores ) && currentScore < Math.max( ...highscores ) ) {
                        highscores.pop()
                        highscores.unshift( currentScore )
                        highscores.sort( function( a, b ) { return b - a } )                        
                        
                        gameState.leaderboard = true
                        
                        sfx.leaderboard.stop()
                        sfx.leaderboard.play()
                        
                    }
                    
                    if ( currentScore > Math.max( ...highscores ) ) {
                        highscores.pop()
                        highscores.unshift( currentScore )
                        highscores.sort( function( a, b ) { return b - a } )                        
                        
                        gameState.highscore = true
                        
                        sfx.highscore.stop()
                        sfx.highscore.play()
                    }
                }, 1 )
            }
            let start = createVector( playerSquare.x, playerSquare.y )
            let end = createVector( playerSquare.x - step, playerSquare.y )
            playerSquare.slider = new Slider( start, end, 'SQUARE' )
            
            playerSquare.x += step
            let randomStep = Math.floor( sfx.move.length * Math.random() )
            sfx.move[ randomStep ].stop()
            sfx.move[ randomStep ].play()
            // sfx.move[ Math.floor( sfx.move.length * Math.random() ) ].play()
            sfx.step.stop()
            sfx.step.play()
        }
        if ( keyCode === controls.circle.UP ) {
            let options = []
            for ( let i = 1; i < tilesCircle.length; i++ ) {
                if ( playerCircle.x === tilesCircle[ i ].x && playerCircle.y < tilesCircle[ i ].y ) {
                    options.push( tilesCircle[ i ].y - playerCircle.y )
                }
            }
            let step = Math.min( ...options )
            if ( options.length > 0 ) {
                tilesCircle[ tilesCircle.length - 1 ].calculateOptions()
                tilesCircle[ tilesCircle.length - 1 ].chooseOption()
                if ( tilesCircle[ 0 ].isCharged ) {
                    playerCircle.isCharged = true
                    tilesCircle[ 0 ].isCharged = !tilesCircle[ 0 ].isCharged
                }
                if ( playerCircle.obstacleAhead ) {
                    if ( !playerCircle.shaking ) {
                        console.log( 'PLAYER TWO DIED' )
                        setTimeout( function() {
                            gameState.gameplay = false
                            gameState.gameover = true
                            sfx.gameOver.stop()
                            sfx.gameOver.play()
                            
                            if ( currentScore > Math.min( ...highscores ) && currentScore < Math.max( ...highscores ) ) {
                                highscores.pop()
                                highscores.unshift( currentScore )
                                highscores.sort( function( a, b ) { return b - a } )                                
                                
                                gameState.leaderboard = true
                                
                                sfx.leaderboard.stop()
                                sfx.leaderboard.play()
                                
                            }
                            
                            if ( currentScore > Math.max( ...highscores ) ) {
                                highscores.pop()
                                highscores.unshift( currentScore )
                                highscores.sort( function( a, b ) { return b - a } )                                
                                
                                gameState.highscore = true
                                
                                sfx.highscore.stop()
                                sfx.highscore.play()
                            }
                        }, 1 )
                    } else {
                        let posX = windowWidth * 0.5 - 0.5 * grid.tileSize * grid.width + obstaclesCircle[ 0 ].cx * grid.tileSize + grid.tileSize * 0.5
                        let posY = windowHeight * 0.5 + 0.5 * grid.tileSize * grid.height - obstaclesCircle[ 0 ].cy * grid.tileSize - grid.tileSize * 0.5 + grid.tileSize * time.offset
                        goBoom( posX, posY )

                        obstaclesCircle = []
                        playerCircle.obstacleAhead = false
                        playerCircle.isCharged = false
                        currentScore += 4000

                        sfx.obstacle.stop()
                        sfx.obstacle.play()
                    }
                }
                tilesCircle.shift()
                currentScore += 1000
            } else {
                console.log( 'PLAYER ONE DIED' )
                setTimeout( function() {
                    gameState.gameplay = false
                    gameState.gameover = true
                    sfx.gameOver.stop()
                    sfx.gameOver.play()
                    
                    if ( currentScore > Math.min( ...highscores ) && currentScore < Math.max( ...highscores ) ) {
                        highscores.pop()
                        highscores.unshift( currentScore )
                        highscores.sort( function( a, b ) { return b - a } )                        
                        
                        gameState.leaderboard = true
                        
                        sfx.leaderboard.stop()
                        sfx.leaderboard.play()
                        
                    }
                    
                    if ( currentScore > Math.max( ...highscores ) ) {
                        highscores.pop()
                        highscores.unshift( currentScore )
                        highscores.sort( function( a, b ) { return b - a } )                        
                        
                        gameState.highscore = true
                        
                        sfx.highscore.stop()
                        sfx.highscore.play()
                    }
                }, 1 )
            }
            let start = createVector( playerCircle.x, playerCircle.y )
            let end = createVector( playerCircle.x, playerCircle.y + step )
            playerCircle.slider = new Slider( start, end, 'CIRCLE' )
            
            playerCircle.y += step
            let randomStep = Math.floor( sfx.move.length * Math.random() )
            sfx.move[ randomStep ].stop()
            sfx.move[ randomStep ].play()
            // sfx.move[ Math.floor( sfx.move.length * Math.random() ) ].play()
            sfx.step.stop()
            sfx.step.play()
        }
        if ( keyCode === controls.circle.DOWN ) {
            let options = []
            for ( let i = 1; i < tilesCircle.length; i++ ) {
                if ( playerCircle.x === tilesCircle[ i ].x && playerCircle.y > tilesCircle[ i ].y ) {
                    options.push( tilesCircle[ i ].y - playerCircle.y )
                }
            }
            let step = Math.max( ...options )
            if ( options.length > 0 ) {
                tilesCircle[ tilesCircle.length - 1 ].calculateOptions()
                tilesCircle[ tilesCircle.length - 1 ].chooseOption()
                if ( tilesCircle[ 0 ].isCharged ) {
                    playerCircle.isCharged = true
                    tilesCircle[ 0 ].isCharged = !tilesCircle[ 0 ].isCharged
                }
                if ( playerCircle.obstacleAhead ) {
                    if ( !playerCircle.shaking ) {
                        console.log( 'PLAYER TWO DIED' )
                        setTimeout( function() {
                            gameState.gameplay = false
                            gameState.gameover = true
                            sfx.gameOver.stop()
                            sfx.gameOver.play()
                            
                            if ( currentScore > Math.min( ...highscores ) && currentScore < Math.max( ...highscores ) ) {
                                highscores.pop()
                                highscores.unshift( currentScore )
                                highscores.sort( function( a, b ) { return b - a } )                                
                                
                                gameState.leaderboard = true
                                
                                sfx.leaderboard.stop()
                                sfx.leaderboard.play()
                                
                            }
                            
                            if ( currentScore > Math.max( ...highscores ) ) {
                                highscores.pop()
                                highscores.unshift( currentScore )
                                highscores.sort( function( a, b ) { return b - a } )                                
                                
                                gameState.highscore = true
                                
                                sfx.highscore.stop()
                                sfx.highscore.play()
                            }
                        }, 1 )
                    } else {
                        let posX = windowWidth * 0.5 - 0.5 * grid.tileSize * grid.width + obstaclesCircle[ 0 ].cx * grid.tileSize + grid.tileSize * 0.5
                        let posY = windowHeight * 0.5 + 0.5 * grid.tileSize * grid.height - obstaclesCircle[ 0 ].cy * grid.tileSize - grid.tileSize * 0.5 + grid.tileSize * time.offset
                        goBoom( posX, posY )

                        obstaclesCircle = []
                        playerCircle.obstacleAhead = false
                        playerCircle.isCharged = false
                        currentScore += 4000

                        sfx.obstacle.stop()
                        sfx.obstacle.play()
                    }
                }
                tilesCircle.shift()
                currentScore += 1000
            } else {
                console.log( 'PLAYER ONE DIED' )
                setTimeout( function() {
                    gameState.gameplay = false
                    gameState.gameover = true
                    sfx.gameOver.stop()
                    sfx.gameOver.play()
                    
                    if ( currentScore > Math.min( ...highscores ) && currentScore < Math.max( ...highscores ) ) {
                        highscores.pop()
                        highscores.unshift( currentScore )
                        highscores.sort( function( a, b ) { return b - a } )                        
                        
                        gameState.leaderboard = true
                        
                        sfx.leaderboard.stop()
                        sfx.leaderboard.play()
                        
                    }
                    
                    if ( currentScore > Math.max( ...highscores ) ) {
                        highscores.pop()
                        highscores.unshift( currentScore )
                        highscores.sort( function( a, b ) { return b - a } )                        
                        
                        gameState.highscore = true
                        
                        sfx.highscore.stop()
                        sfx.highscore.play()
                    }
                }, 1 )
            }
            let start = createVector( playerCircle.x, playerCircle.y )
            let end = createVector( playerCircle.x, playerCircle.y + step )
            playerCircle.slider = new Slider( start, end, 'CIRCLE' )
            
            playerCircle.y += step
            let randomStep = Math.floor( sfx.move.length * Math.random() )
            sfx.move[ randomStep ].stop()
            sfx.move[ randomStep ].play()
            // sfx.move[ Math.floor( sfx.move.length * Math.random() ) ].play()
            sfx.step.stop()
            sfx.step.play()
        }
        if ( keyCode === controls.circle.RIGHT && playerCircle.x < 17 ) {
            let options = []
            for ( let i = 1; i < tilesCircle.length; i++ ) {
                if ( playerCircle.y === tilesCircle[ i ].y && playerCircle.x < tilesCircle[ i ].x ) {
                    options.push( tilesCircle[ i ].x - playerCircle.x )
                }
            }
            let step = Math.min( ...options )
            if ( options.length > 0 ) {
                tilesCircle[ tilesCircle.length - 1 ].calculateOptions()
                tilesCircle[ tilesCircle.length - 1 ].chooseOption()
                if ( tilesCircle[ 0 ].isCharged ) {
                    playerCircle.isCharged = true
                    tilesCircle[ 0 ].isCharged = !tilesCircle[ 0 ].isCharged
                }
                if ( playerCircle.obstacleAhead ) {
                    if ( !playerCircle.shaking ) {
                        console.log( 'PLAYER TWO DIED' )
                        setTimeout( function() {
                            gameState.gameplay = false
                            gameState.gameover = true
                            sfx.gameOver.stop()
                            sfx.gameOver.play()
                            
                            if ( currentScore > Math.min( ...highscores ) && currentScore < Math.max( ...highscores ) ) {
                                highscores.pop()
                                highscores.unshift( currentScore )
                                highscores.sort( function( a, b ) { return b - a } )                                
                                
                                gameState.leaderboard = true
                                
                                sfx.leaderboard.stop()
                                sfx.leaderboard.play()
                                
                            }
                            
                            if ( currentScore > Math.max( ...highscores ) ) {
                                highscores.pop()
                                highscores.unshift( currentScore )
                                highscores.sort( function( a, b ) { return b - a } )                                
                                
                                gameState.highscore = true
                                
                                sfx.highscore.stop()
                                sfx.highscore.play()
                            }
                        }, 1 )
                    } else {
                        let posX = windowWidth * 0.5 - 0.5 * grid.tileSize * grid.width + obstaclesCircle[ 0 ].cx * grid.tileSize + grid.tileSize * 0.5
                        let posY = windowHeight * 0.5 + 0.5 * grid.tileSize * grid.height - obstaclesCircle[ 0 ].cy * grid.tileSize - grid.tileSize * 0.5 + grid.tileSize * time.offset
                        goBoom( posX, posY )

                        obstaclesCircle = []
                        playerCircle.obstacleAhead = false
                        playerCircle.isCharged = false
                        currentScore += 4000

                        sfx.obstacle.stop()
                        sfx.obstacle.play()
                    }
                }
                tilesCircle.shift()
                currentScore += 1000
            } else {
                console.log( 'PLAYER ONE DIED' )
                setTimeout( function() {
                    gameState.gameplay = false
                    gameState.gameover = true
                    sfx.gameOver.stop()
                    sfx.gameOver.play()
                    
                    if ( currentScore > Math.min( ...highscores ) && currentScore < Math.max( ...highscores ) ) {
                        highscores.pop()
                        highscores.unshift( currentScore )
                        highscores.sort( function( a, b ) { return b - a } )                        
                        
                        gameState.leaderboard = true
                        
                        sfx.leaderboard.stop()
                        sfx.leaderboard.play()
                        
                    }
                    
                    if ( currentScore > Math.max( ...highscores ) ) {
                        highscores.pop()
                        highscores.unshift( currentScore )
                        highscores.sort( function( a, b ) { return b - a } )                        
                        
                        gameState.highscore = true
                        
                        sfx.highscore.stop()
                        sfx.highscore.play()
                    }
                }, 1 )
            }
            let start = createVector( playerCircle.x, playerCircle.y )
            let end = createVector( playerCircle.x - step, playerCircle.y )
            playerCircle.slider = new Slider( start, end, 'CIRCLE' )
            
            playerCircle.x += step
            let randomStep = Math.floor( sfx.move.length * Math.random() )
            sfx.move[ randomStep ].stop()
            sfx.move[ randomStep ].play()
            // sfx.move[ Math.floor( sfx.move.length * Math.random() ) ].play()
            sfx.step.stop()
            sfx.step.play()
        }
        if ( keyCode === controls.circle.LEFT && playerCircle.x > 0 ) {
            let options = []
            for ( let i = 1; i < tilesCircle.length; i++ ) {
                if ( playerCircle.y === tilesCircle[ i ].y && playerCircle.x > tilesCircle[ i ].x ) {
                    options.push( tilesCircle[ i ].x - playerCircle.x )
                }
            }
            let step = Math.max( ...options )
            if ( options.length > 0 ) {
                tilesCircle[ tilesCircle.length - 1 ].calculateOptions()
                tilesCircle[ tilesCircle.length - 1 ].chooseOption()
                if ( tilesCircle[ 0 ].isCharged ) {
                    playerCircle.isCharged = true
                    tilesCircle[ 0 ].isCharged = !tilesCircle[ 0 ].isCharged
                }
                if ( playerCircle.obstacleAhead ) {
                    if ( !playerCircle.shaking ) {
                        console.log( 'PLAYER TWO DIED' )
                        setTimeout( function() {
                            gameState.gameplay = false
                            gameState.gameover = true
                            sfx.gameOver.stop()
                            sfx.gameOver.play()
                            
                            if ( currentScore > Math.min( ...highscores ) && currentScore < Math.max( ...highscores ) ) {
                                highscores.pop()
                                highscores.unshift( currentScore )
                                highscores.sort( function( a, b ) { return b - a } )                                
                                
                                gameState.leaderboard = true
                                
                                sfx.leaderboard.stop()
                                sfx.leaderboard.play()
                                
                            }
                            
                            if ( currentScore > Math.max( ...highscores ) ) {
                                highscores.pop()
                                highscores.unshift( currentScore )
                                highscores.sort( function( a, b ) { return b - a } )                                
                                
                                gameState.highscore = true
                                
                                sfx.highscore.stop()
                                sfx.highscore.play()
                            }
                        }, 1 )
                    } else {
                        let posX = windowWidth * 0.5 - 0.5 * grid.tileSize * grid.width + obstaclesCircle[ 0 ].cx * grid.tileSize + grid.tileSize * 0.5
                        let posY = windowHeight * 0.5 + 0.5 * grid.tileSize * grid.height - obstaclesCircle[ 0 ].cy * grid.tileSize - grid.tileSize * 0.5 + grid.tileSize * time.offset
                        goBoom( posX, posY )

                        obstaclesCircle = []
                        playerCircle.obstacleAhead = false
                        playerCircle.isCharged = false
                        currentScore += 4000

                        sfx.obstacle.stop()
                        sfx.obstacle.play()
                    }
                }
                tilesCircle.shift()
                currentScore += 1000
            } else {
                console.log( 'PLAYER ONE DIED' )
                setTimeout( function() {
                    gameState.gameplay = false
                    gameState.gameover = true
                    sfx.gameOver.stop()
                    sfx.gameOver.play()
                    
                    if ( currentScore > Math.min( ...highscores ) && currentScore < Math.max( ...highscores ) ) {
                        highscores.pop()
                        highscores.unshift( currentScore )
                        highscores.sort( function( a, b ) { return b - a } )                        
                        
                        gameState.leaderboard = true
                        
                        sfx.leaderboard.stop()
                        sfx.leaderboard.play()
                        
                    }
                    
                    if ( currentScore > Math.max( ...highscores ) ) {
                        highscores.pop()
                        highscores.unshift( currentScore )
                        highscores.sort( function( a, b ) { return b - a } )                        
                        
                        gameState.highscore = true
                        
                        sfx.highscore.stop()
                        sfx.highscore.play()
                    }
                }, 1 )
            }
            let start = createVector( playerCircle.x, playerCircle.y )
            let end = createVector( playerCircle.x - step, playerCircle.y )
            playerCircle.slider = new Slider( start, end, 'CIRCLE' )
            
            playerCircle.x += step
            let randomStep = Math.floor( sfx.move.length * Math.random() )
            sfx.move[ randomStep ].stop()
            sfx.move[ randomStep ].play()
            // sfx.move[ Math.floor( sfx.move.length * Math.random() ) ].play()
            sfx.step.stop()
            sfx.step.play()
        }
        if ( keyCode === controls.circle.ENTER ) {
            if ( playerCircle.isCharged ) {
                playerCircle.shaking = true
            }
            if ( tilesCircle.length === 1 && tilesSquare.length === 1 && playerCircle.timeToSwap && playerSquare.timeToSwap ) {
                let start = createVector( playerCircle.x, playerCircle.y )
                let end = createVector( playerSquare.x, playerSquare.y )
                playerCircle.swapAnimation = new swapAnimation( start, end )
                playerSquare.swapAnimation = new swapAnimation( start, end )
                
                let tempX = playerCircle.x
                let tempY = playerCircle.y
                playerCircle.x = playerSquare.x
                playerCircle.y = playerSquare.y
                playerSquare.x = tempX
                playerSquare.y = tempY
                swaps = []
                
                let tempTileX = tilesCircle[ 0 ].x
                let tempTileY = tilesCircle[ 0 ].y
                tilesCircle[ 0 ].x = tilesSquare[ 0 ].x
                tilesCircle[ 0 ].y = tilesSquare[ 0 ].y
                tilesSquare[ 0 ].x = tempTileX
                tilesSquare[ 0 ].y = tempTileY
                
                tilesCircle[ tilesCircle.length - 1 ].isSwapTile = false
                tilesSquare[ tilesSquare.length - 1 ].isSwapTile = false
                
                playerCircle.timeToSwap = false
                playerSquare.timeToSwap = false
                
                tilesCircle[ tilesCircle.length - 1 ].calculateOptions()
                tilesSquare[ tilesSquare.length - 1 ].calculateOptions()
                tilesCircle[ tilesCircle.length - 1 ].chooseOption()
                tilesSquare[ tilesSquare.length - 1 ].chooseOption()
                
                tilesCircle[ tilesCircle.length - 1 ].calculateOptions()
                tilesSquare[ tilesSquare.length - 1 ].calculateOptions()
                tilesCircle[ tilesCircle.length - 1 ].chooseOption()
                tilesSquare[ tilesSquare.length - 1 ].chooseOption()
                
                tilesCircle[ tilesCircle.length - 1 ].calculateOptions()
                tilesSquare[ tilesSquare.length - 1 ].calculateOptions()
                
                currentScore += 9000

                playerCircle.swapTime = undefined
                playerSquare.swapTime = undefined
                
                sfx.swap.stop()
                sfx.swap.play()
            }
        }
        if ( keyCode === controls.square.ENTER ) {
            if ( playerSquare.isCharged ) {
                playerSquare.shaking = true
            }
            if ( tilesCircle.length === 1 && tilesSquare.length === 1 && playerCircle.timeToSwap && playerSquare.timeToSwap ) {
                let start = createVector( playerCircle.x, playerCircle.y )
                let end = createVector( playerSquare.x, playerSquare.y )
                playerCircle.swapAnimation = new swapAnimation( start, end )
                playerSquare.swapAnimation = new swapAnimation( start, end )
                
                let tempX = playerCircle.x
                let tempY = playerCircle.y
                playerCircle.x = playerSquare.x
                playerCircle.y = playerSquare.y
                playerSquare.x = tempX
                playerSquare.y = tempY
                swaps = []
                
                let tempTileX = tilesCircle[ 0 ].x
                let tempTileY = tilesCircle[ 0 ].y
                tilesCircle[ 0 ].x = tilesSquare[ 0 ].x
                tilesCircle[ 0 ].y = tilesSquare[ 0 ].y
                tilesSquare[ 0 ].x = tempTileX
                tilesSquare[ 0 ].y = tempTileY
                
                tilesCircle[ tilesCircle.length - 1 ].isSwapTile = false
                tilesSquare[ tilesSquare.length - 1 ].isSwapTile = false
                
                playerCircle.timeToSwap = false
                playerSquare.timeToSwap = false
                
                tilesCircle[ tilesCircle.length - 1 ].calculateOptions()
                tilesSquare[ tilesSquare.length - 1 ].calculateOptions()
                tilesCircle[ tilesCircle.length - 1 ].chooseOption()
                tilesSquare[ tilesSquare.length - 1 ].chooseOption()
                
                tilesCircle[ tilesCircle.length - 1 ].calculateOptions()
                tilesSquare[ tilesSquare.length - 1 ].calculateOptions()
                tilesCircle[ tilesCircle.length - 1 ].chooseOption()
                tilesSquare[ tilesSquare.length - 1 ].chooseOption()
                
                tilesCircle[ tilesCircle.length - 1 ].calculateOptions()
                tilesSquare[ tilesSquare.length - 1 ].calculateOptions()
                
                currentScore += 9000

                playerCircle.swapTime = undefined
                playerSquare.swapTime = undefined
                
                sfx.swap.stop()
                sfx.swap.play()
            }
        }
        
        tilesCircle[ tilesCircle.length - 1 ].calculateOptions()
        tilesSquare[ tilesSquare.length - 1 ].calculateOptions()
    }
    
    if ( gameState.gameover ) {
        if ( keyCode === controls.circle.ENTER ) {
            gameState.gameover = false
            gameState.highscores = true

            let randomStep = Math.floor( sfx.move.length * Math.random() )
            sfx.move[ randomStep ].stop()
            sfx.move[ randomStep ].play()
            // sfx.move[ Math.floor( sfx.move.length * Math.random() ) ].play()
            sfx.step.stop()
            sfx.step.play()
        }
        if ( keyCode === controls.square.ENTER ) {
            gameState.gameover = false
            gameState.highscores = true
            
            let randomStep = Math.floor( sfx.move.length * Math.random() )
            sfx.move[ randomStep ].stop()
            sfx.move[ randomStep ].play()
            // sfx.move[ Math.floor( sfx.move.length * Math.random() ) ].play()
            sfx.step.stop()
            sfx.step.play()
        }
    }
}

function keyReleased() {    
    if ( gameState.gameplay ) {
        if ( tilesCircle[ 0 ].isCharged ) {
            playerCircle.isCharged = true
            tilesCircle[ 0 ].isCharged = !tilesCircle[ 0 ].isCharged
            sfx.charged.stop()
            sfx.charged.play()
        }
        if ( tilesSquare[ 0 ].isCharged ) {
            playerSquare.isCharged = true
            tilesSquare[ 0 ].isCharged = !tilesSquare[ 0 ].isCharged
            sfx.charged.stop()
            sfx.charged.play()
        }
        
        if ( keyCode === controls.circle.UP || keyCode === controls.circle.RIGHT || keyCode === controls.circle.DOWN || keyCode === controls.circle.LEFT || keyCode === controls.circle.ENTER ) {
            if ( swaps.length === 0 ) {
                if ( playerCircle.isCharged && obstaclesCircle.length === 0 ) {
                    if ( Math.random() <= 0.5 ) {
                        let s = tilesCircle[ tilesCircle.length - 2 ]
                        let e = tilesCircle[ tilesCircle.length - 1 ]
                        obstaclesCircle.push( new Obstacle( s.x, s.y, e.x, e.y ) )
                    }
                }
            }
            if ( obstaclesCircle.length !== 0 ) {
                if ( playerCircle.x === obstaclesCircle[ 0 ].x1 && playerCircle.y === obstaclesCircle[ 0 ].y1 ) {
                    playerCircle.obstacleAhead = true
                    // console.log( 'BEWARE! OBSTACLE AHEAD')
                }
            }
        }
        if ( keyCode === controls.square.UP || keyCode === controls.square.RIGHT || keyCode === controls.square.DOWN || keyCode === controls.square.LEFT || keyCode === controls.square.ENTER ) {
            if ( swaps.length === 0 ) {
                if ( playerSquare.isCharged && obstaclesSquare.length === 0 ) {
                    if ( Math.random() <= 0.5 ) {
                        let s = tilesSquare[ tilesSquare.length - 2 ]
                        let e = tilesSquare[ tilesSquare.length - 1 ]
                        obstaclesSquare.push( new Obstacle( s.x, s.y, e.x, e.y ) )
                    }
                }
                
                if ( obstaclesSquare.length !== 0 ) {
                    if ( playerSquare.x === obstaclesSquare[ 0 ].x1 && playerSquare.y === obstaclesSquare[ 0 ].y1 ) {
                        playerSquare.obstacleAhead = true
                        // console.log( 'BEWARE! OBSTACLE AHEAD')
                    }
                }
            }
        }
        
        if ( keyCode === controls.circle.ENTER ) {
            playerCircle.shaking = false
        }
        if ( keyCode === controls.square.ENTER ) {
            playerSquare.shaking = false
        }
        
        if ( obstaclesCircle.length > 1 ) {
            obstaclesCircle.shift()
        }
        if ( obstaclesSquare.length > 1 ) {
            obstaclesSquare.shift()
        }
        
        tilesCircle[ tilesCircle.length - 1 ].calculateOptions()
        tilesSquare[ tilesSquare.length - 1 ].calculateOptions()
    }
}

function windowResized() {
    if ( windowWidth >= windowHeight * 9 / 16 ) {
        screenSpace.height = windowHeight
        screenSpace.width = screenSpace.height * 9 / 16
    } else {
        screenSpace.width = windowWidth
        screenSpace.height = screenSpace.width * 16 / 9
    }
    grid.tileSize = screenSpace.width / grid.width
    
    resizeCanvas( windowWidth, windowHeight )
}

function shuffle( array ) {
    let currentIndex = array.length, randomIndex
    while ( currentIndex > 0 ) {
        randomIndex = Math.floor( Math.random() * currentIndex )
        currentIndex--
        [ array [ currentIndex ], array[ randomIndex ] ] = [ array[ randomIndex ], array[ currentIndex ] ]
    }
    return array
}

function allAreFalse( arr ) {
    return arr.every( element => element === false )
}

function convertCoordinates( position ) {
    let worldPosition = createVector( 0, 0 )
    worldPosition.x = windowWidth * 0.5 - 0.5 * grid.tileSize * grid.width + grid.tileSize * position.x
    worldPosition.y = windowHeight * 0.5 + 0.5 * grid.tileSize * grid.height - grid.tileSize * position.y
    return worldPosition
}

function star( x, y, radius1, radius2, npoints ) {
    let angle = TWO_PI / npoints
    let halfAngle = angle / 2.0
    beginShape()
    for ( let a = 0; a < TWO_PI; a += angle ) {
        let sx = x + cos( a ) * radius2
        let sy = y + sin( a ) * radius2
        vertex( sx, sy )
        sx = x + cos( a + halfAngle ) * radius1
        sy = y + sin( a + halfAngle ) * radius1
        vertex( sx, sy )
    }
    endShape( CLOSE )
}

function goBoom( xpos, ypos ) {
    for ( var i = 0; i < 20; i++ ){
        var newpoint = { 
            x: xpos + random( -grid.tileSize * 0.2, grid.tileSize * 0.2 ), 
            y: ypos + random( -grid.tileSize * 0.2, grid.tileSize * 0.2 ), 
            xdrift: random( -grid.tileSize, grid.tileSize ),
            ydrift: random( -grid.tileSize, grid.tileSize ),
            age: 1
        }
        points.push( newpoint )
    }
}